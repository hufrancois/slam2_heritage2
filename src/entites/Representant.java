package entites;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Representant extends Salarie{

    private Float tauxComRep;
    private Float plafondFraisDep;
    
    private List<Vente> lesVentes=new LinkedList();

    public Float ca(){
       
       Float somme=0F;
       for ( Vente v : lesVentes){ somme+=v.getMontantFact();}
       return somme;
    }
    
    public Float totalFraisDep(){
       
       Float somme=0F;
       for ( Vente v : lesVentes){ somme+=v.getFraisDep();}
       return somme;
    }
    
    public Float commission(){
       
       Float com=0F;
       
       if(ca()<10000)               { com = ca()*0.02f; }
       else if(ca()<20000)          { com = 200 + (ca()-10000)*0.04f; }
       else                         { com = 600 +(ca()-20000)*0.06f; }
       
       com = com*(1+tauxComRep);
       return com;
    }
    
    public Float salaireBrut(){
       
       Float salaire=0F;
       
       salaire = getSalaireDeBase()+commission()+totalFraisDep();
       
       return salaire;
    }
    
    public void afficher(){ System.out.printf(getMatricule(), getNomprenom(), ca(), totalFraisDep(), commission(), salaireBrut()); }
    
    //<editor-fold defaultstate="collapsed" desc="gets & sets">
    
    public Float getTauxComRep() {
        return tauxComRep;
    }
    
    public Float getPlafondFraisDep() {
        return plafondFraisDep;
    }
    
    public void setTauxComRep(Float tauxComRep) {
        this.tauxComRep = tauxComRep;
    }
    
    public void setPlafondFraisDep(Float plafondFraisDep) {
        this.plafondFraisDep = plafondFraisDep;
    }
    
     public List<Vente> getLesVentes() {
        return lesVentes;
    }

    public void setLesVentes(List<Vente> lesVentes) {
        this.lesVentes = lesVentes;
    }
    //</editor-fold>   
}
