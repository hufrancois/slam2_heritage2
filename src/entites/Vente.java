package entites;

import java.util.Date;

public class Vente {

    // Attributs informationnels
    private Long  numFact;
    private Date  dateFact;
    private Float montantFact;
    private Float fraisDep;
    
    //<editor-fold defaultstate="collapsed" desc="gets & sets">
   
    public Long getNumFact() {
        return numFact;
    }
    
    public Date getDateFact() {
        return dateFact;
    }
    
    public Float getMontantFact() {
        return montantFact;
    }
    
    public Float getFraisDep() {
        return fraisDep;
    }
    
    public void setNumFact(Long numFact) {
        this.numFact = numFact;
    }
    
    public void setDateFact(Date dateFact) {
        this.dateFact = dateFact;
    }
    
    public void setMontantFact(Float montantFact) {
        this.montantFact = montantFact;
    }
    
    public void setFraisDep(Float fraisDep) {
        this.fraisDep = fraisDep;
    }
    //</editor-fold>
}
