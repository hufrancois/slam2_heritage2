package entites;
import java.util.LinkedList;
import java.util.List;

public class SecteurGeo {

    // Attribut informationnels
    private String         codeSecteur;
    private String         libSecteur;
    
    // Attribut navigationnel
    private List<Salarie>  lesSalaries=new LinkedList();

    public Float sommeSalaires(){
        
        Float sommeSal=0F;
        for(  Salarie  sal  :  lesSalaries){  sommeSal  += sal.salaireBrut(); }
        return sommeSal;
    }
  
    //<editor-fold defaultstate="collapsed" desc="Get & Set">
    
    public String getCodeSecteur() {
        return codeSecteur;
    }
    
    public void setCodeSecteur(String codeSecteur) {
        this.codeSecteur = codeSecteur;
    }
    
    public String getLibSecteur() {
        return libSecteur;
    }
    
    public void setLibSecteur(String libSecteur) {
        this.libSecteur = libSecteur;
    }
    
    public List<Salarie> getLesSalaries() {
        return lesSalaries;
    }
    
    public void setLesSalaries(List<Salarie> lesSalaries) {
        this.lesSalaries = lesSalaries;
    }
    //</editor-fold> 
}
