
package data;

import entites.Representant;
import entites.Salarie;
import entites.SecteurGeo;
import entites.Vente;
import java.util.LinkedList;
import java.util.List;
import static utilitaires.UtilDate.chaineVersDate;

public class Dao {

 private static List<SecteurGeo>  tousLesSecteursGeo = new LinkedList();  
 private static List<Salarie>          tousLesSalaries    = new LinkedList();
    
 static{   
    
     //Initialisation des données
     
   SecteurGeo     sg1= new SecteurGeo();
   SecteurGeo     sg2= new SecteurGeo();
   
   Salarie        sl1= new Salarie();
   Salarie        sl2= new Salarie(); 
   Salarie        sl3= new Salarie();
   Salarie        sl4= new Salarie(); 
   
   
   Representant   rp1= new Representant(); 
   Representant   rp2= new Representant();  
   Representant   rp3= new Representant(); 
  
   Vente          vt1= new Vente();
   Vente          vt2= new Vente();
   Vente          vt3= new Vente();
   Vente          vt4= new Vente();
   Vente          vt5= new Vente();
   Vente          vt6= new Vente();
   Vente          vt7= new Vente();
   Vente          vt8= new Vente();

   sg1.getLesSalaries().add(sl1);
   sg1.getLesSalaries().add(sl2);
   sg2.getLesSalaries().add(sl3);
   sg2.getLesSalaries().add(sl4);
   
   sl1.setLeSecteurGeo(sg1);
   sl2.setLeSecteurGeo(sg1);
   sl3.setLeSecteurGeo(sg2);
   sl4.setLeSecteurGeo(sg2);
   
   sg1.getLesSalaries().add(rp1);
   sg1.getLesSalaries().add(rp2);
   sg2.getLesSalaries().add(rp3);
   
   rp1.setLeSecteurGeo(sg1);
   rp2.setLeSecteurGeo(sg1);
   rp3.setLeSecteurGeo(sg2);
   
   rp1.getLesVentes().add(vt1);
   rp1.getLesVentes().add(vt2);
   rp1.getLesVentes().add(vt3);
   
   rp2.getLesVentes().add(vt4);
   rp2.getLesVentes().add(vt5);
  
   rp3.getLesVentes().add(vt6);
   rp3.getLesVentes().add(vt7);
   rp3.getLesVentes().add(vt8);
   
   tousLesSecteursGeo.add(sg1); tousLesSecteursGeo.add(sg2);
   
   tousLesSalaries.add(sl1);tousLesSalaries.add(sl2);tousLesSalaries.add(sl3);tousLesSalaries.add(sl4);
   tousLesSalaries.add(rp1);tousLesSalaries.add(rp2);tousLesSalaries.add(rp3);
   
   sg1.setCodeSecteur("NPDC"); sg1.setLibSecteur("Nord Pas de Calais");
   sg2.setCodeSecteur("CA")  ; sg2.setLibSecteur("Champagne Ardennes");
 
   sl1.setMatricule("S001");sl1.setNomprenom("Delerue Jean")  ;sl1.setSexe("M") ;sl1.setSalaireDeBase(1750F);
   sl2.setMatricule("S002");sl2.setNomprenom("Martin  Sophie");sl2.setSexe("F") ;sl2.setSalaireDeBase(1880F);
   sl3.setMatricule("S003");sl3.setNomprenom("Leroux Pierre")  ;sl3.setSexe("M") ;sl3.setSalaireDeBase(1925F);
   sl4.setMatricule("S004");sl4.setNomprenom("Grébert  Sandrine");sl4.setSexe("F") ;sl4.setSalaireDeBase(1730F);
 

   rp1.setMatricule("R001");rp1.setNomprenom("Lefort Alain")  ;rp1.setSexe("M") ;rp1.setSalaireDeBase(1650F);
   rp1.setPlafondFraisDep(500F);rp1.setTauxComRep(0.05F);
   
   rp2.setMatricule("R002");rp2.setNomprenom("Dumortier Béatrice");rp2.setSexe("F") ;rp2.setSalaireDeBase(1780F);
   rp2.setPlafondFraisDep(800F);rp2.setTauxComRep(0.07F);
   
   rp3.setMatricule("R003");rp3.setNomprenom("Fortier Daniel")  ;rp3.setSexe("M") ;rp3.setSalaireDeBase(1625F);
   rp3.setPlafondFraisDep(400F);rp3.setTauxComRep(0.03F);
 
   vt1.setNumFact(11001L);vt1.setMontantFact(2876.5F) ; vt1.setFraisDep(138.75F);
   vt1.setDateFact(chaineVersDate("02/02/2016"));
   vt2.setNumFact(11012L);vt2.setMontantFact(1576.5F) ; vt2.setFraisDep( 88.65F);
   vt2.setDateFact(chaineVersDate("05/02/2016"));
   vt3.setNumFact(11021L);vt3.setMontantFact(1233.55F); vt3.setFraisDep(235.95F);
   vt3.setDateFact(chaineVersDate("04/02/2016"));
   vt4.setNumFact(11033L);vt4.setMontantFact( 576.5F) ; vt4.setFraisDep( 88.35F);
   vt4.setDateFact(chaineVersDate("03/02/2016"));
   vt5.setNumFact(11075L);vt5.setMontantFact(1872.85F); vt5.setFraisDep(188.25F);
   vt5.setDateFact(chaineVersDate("05/02/2016"));
   vt6.setNumFact(11089L);vt6.setMontantFact(3377.35F); vt6.setFraisDep(285.15F);
   vt6.setDateFact(chaineVersDate("04/02/2016"));
   vt7.setNumFact(11107L);vt7.setMontantFact(1676.25F); vt7.setFraisDep(155.45F);
   vt7.setDateFact(chaineVersDate("04/02/2016"));
   vt8.setNumFact(11122L);vt8.setMontantFact(2576.5F);  vt8.setFraisDep(235.65F);
   vt8.setDateFact(chaineVersDate("05/02/2016"));
 }

 public static List<SecteurGeo> getTousLesSecteursGeo() {
        return tousLesSecteursGeo;
 }

 public static SecteurGeo          getLeSecteurGeo(String codeSecteur){
 
     SecteurGeo sg=null;
     
     // Pour chaque SecteurGeo designé par la variable  s  dans (:) la liste
     
     for (SecteurGeo s : tousLesSecteursGeo){
         
         if (s.getCodeSecteur().equals(codeSecteur) ){
            
            sg=s;
            break;
         }
     }
     return sg;
 }
 
 public static List<Salarie>        getTousLesSalaries() {
        return tousLesSalaries;
 }
 
 public static Salarie                 getLeSalarie(String matricule){
 
     Salarie s=null;  
     for (Salarie sal : tousLesSalaries){
      
         if (sal.getMatricule().equals(matricule) ){
            
            s=sal;
            break;
         }
     }
     return s;
 }
 
}
